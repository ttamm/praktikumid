package praktikum6;

import lib.TextIO;
import praktikum5.KaksVahemikkuVol2;

public class KullKiriPanustega {

	public static void main(String[] args) {

		int kasutajaRaha = 100;

		while (kasutajaRaha > 0) {
			System.out.println("raha jääk " + kasutajaRaha);
			System.out.println("tee oma panus - max 25 ühikut");
			int maxpanus = Math.min(kasutajaRaha,  25);
			int panus = Meetodid.kasutajaSisestus(1, 25);
			kasutajaRaha -= panus;

			int myndivise = SajaniArvamine.suvaline(0, 1);

			if (myndivise == 0) { // kull
				System.out.println("võitsid, saad raha topelt tagasi!");
				kasutajaRaha += panus * 2;
			} else { // kiri
				System.out.println("kaotasid, mäng läbi");
			}

		}
	}

}
