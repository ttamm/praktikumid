package praktikum6;

import lib.TextIO;

public class SajaniArvamine {

	public static void main(String[] args) {

		for (int i = 0; i < 10; i++) {
			int arvuti = suvaline(1, 100);
			System.out.println("arva ära number ");

			while (true) {
				int kasutaja = TextIO.getlnInt();
				if (arvuti == kasutaja) {
					System.out.println("arvasid ära!");
				} else if (arvuti > kasutaja) {
					System.out.println("pakkumine liiga väike, arva uuesti");
				} else {
					System.out.println("pakkumine liiga suur, arva uuesti");
				}
			}
		}

	}

	public static int suvaline(int min, int max) {
		// Math.random(); // nullist üheni
		// Math.random() * 100; // nullist sajani
		int vahemik = max - min + 1;
		return (int) (Math.random() * vahemik);

	}
}
