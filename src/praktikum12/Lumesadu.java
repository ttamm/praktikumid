package praktikum12;

import java.util.ArrayList;

public class Lumesadu {

	ArrayList<GraafikaNaide> lumehelbed;

	public Lumesadu(int helvesteArv, int aknaLaius, int aknaK6rgus) {

		lumehelbed = new ArrayList<GraafikaNaide>();

		for (int i = 0; i < helvesteArv; i++) {

			int x = (int) (aknaLaius * Math.random());

			int y = (int) (aknaK6rgus * Math.random());

			GraafikaNaide l = new GraafikaNaide();

			lumehelbed.add(l);

		}

	}

	public ArrayList<GraafikaNaide> annaHelbed() {

		return lumehelbed;

	}

}
