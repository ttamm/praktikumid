package praktikum12;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.stage.Stage;

public class GraafikaNaide extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("JavaFX-iga joonistamise näide");
		Group root = new Group();
		Canvas canvas = new Canvas(1000, 1000);
		GraphicsContext gc = canvas.getGraphicsContext2D();
		joonista(gc);
		root.getChildren().add(canvas);
		primaryStage.setScene(new Scene(root));
		primaryStage.show();
	}

	private void joonista(GraphicsContext gc) {
		//LumeHelves l = new LumeHelves(100, 200);
		//l.joonistaMind(gc);
		Naerunägu l = new Naerunägu(50, 50);
		l.joonistaMind(gc);
		
		Naerunägu l2 = new Naerunägu(500, 500);
		l2.joonistaMind(gc);
		
		Naerunägu l3 = new Naerunägu(50, 500);
		l3.joonistaMind(gc);


	}

}