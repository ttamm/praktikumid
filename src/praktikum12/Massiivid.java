package praktikum12;

public class Massiivid {

	public static void main(String[] args) {
		//int[] minuMassiiv = { 2, 3, 8, 6 };
		//tryki(minuMassiiv);

		int[][] neo = { { 1, 1, 1, 1, 1 }, 
				        { 2, 3, 4, 5, 6 }, 
				        { 3, 4, 5, 6, 7 }, 
				        { 4, 5, 6, 7, 8 },
				        { 5, 6, 7, 8, 9 }, };
		//tryki(neo);
		// tryki(ridadeSummad(neo));
		// System.out.println(kõrvalDiagonaaliSumma(neo));
		//tryki(ridadeMaksimumid(neo)); // sysoutiga tuli mäluaadress?
		System.out.println(miinimum(neo));

	}

	public static int miinimum(int[][] maatriks) {
		int seniVäikseim = Integer.MAX_VALUE;
		for (int[] rida : maatriks) {
			for (int number : rida) {
				if (number < seniVäikseim) {
					seniVäikseim = number;
				}
			}

		}
		return seniVäikseim;
	}
	//// public static int reaMiinimum(int[] rida) {
	// int seniVäikseim = rida [4];
	// for (int i = 0; i < rida.length; i++) {
	// if (seniVäikseim > rida[i]) {
	// seniVäikseim = rida[i];
	// }
	// }
	// return seniVäikseim;
	// }

	public static int[] ridadeMaksimumid(int[][] maatriks) {
		int[] maksimumid = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			maksimumid[i] = reaMaksimum(maatriks[i]);

		}
		return maksimumid;
	}

	public static int reaMaksimum(int[] rida) {
		int seniSuurim = rida[0]; // algselt on maksimum rea esimene arv
		for (int i = 0; i < rida.length; i++) {
			if (seniSuurim < rida[i]) {
				seniSuurim = rida[i];
			}
		}

		return seniSuurim;
	}

	public static int kõrvalDiagonaaliSumma(int[][] maatriks) {
		int summa = 0;
		// for (int i = 0; i < maatriks.length; i++) {
		// for (int j = 0; j < maatriks[i].length; j++) { // i ja j tähistavad
		// rea indekseid
		// if ( i == j) { // (peadiagonaali leidmiseks) kui rea indeksid on
		// samad, saab arvutada peadiagonaali summa
		// summa += maatriks [i][j];
		// }

		// }
		// }
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks.length; j++) {
				if (i + j == maatriks.length - 1) { // maatriksi reapikkus
													// miinus üks
					summa += maatriks[i][j];
				}
			}

		}
		return summa;

	}

	public static int[] ridadeSummad(int[][] maatriks) {
		int[] summad = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			summad[i] = reaSumma(maatriks[i]);
		}
		return summad;
	}

	public static int reaSumma(int[] rida) {
		int summa = 0;
		for (int i : rida) {
			summa += i;
		}
		return summa;
	}

	public static void tryki(int[][] maatriks) {
		System.out.println();
		System.out.print("kahemõõtmeline trükimeetod: ");
		for (int i = 0; i < maatriks.length; i++) {
			tryki(maatriks[i]); // iga kord kutsub eelmise meetodi välja
		}
		System.out.println();
	}

	public static void tryki(int[] massiiv) {

		for (int i = 0; i < massiiv.length; i++) {
			System.out.print(massiiv[i] + " ");
		}

	}

}
