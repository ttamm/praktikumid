package praktikum12;

import javafx.scene.canvas.GraphicsContext;

public class LumeHelves {
	int x, y;
	public LumeHelves(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public void joonistaMind(GraphicsContext gc) {
		gc.fillText("Joonista midagi", x, y);
	}

}
