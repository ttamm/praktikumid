package praktikum12;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;

public class Naerunägu {

	int x;
	int y;

	public Naerunägu(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void joonistaMind(GraphicsContext gc) {
		// Määrame värvid
		gc.setFill(Color.GREEN);
		gc.setStroke(Color.BLUE);
		// Joone laius
		gc.setLineWidth(5);

		// Tegelase roheline pea
		gc.fillRoundRect(x, y, 300, 300, 40, 40);

		// Suu
		//gc.strokeLine(100,100, 300, 300);
		gc.strokeArc(x + 50, y + 170, 200, 100, 360, -180, ArcType.ROUND);
		// Silmad
		gc.strokeOval(x + 50, y + 50, 50, 50);
		gc.strokeOval(x + 200,y + 50, 50, 50);

		// Värvivahetus
		gc.setFill(Color.RED);

		// Punane nina
		gc.fillRoundRect(x + 125, y + 140, 50, 50, 10, 10);
	}

}
