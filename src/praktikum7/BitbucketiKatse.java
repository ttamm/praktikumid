package praktikum7;

public class BitbucketiKatse {

	public static void main(String[] args) {
		// - muudatus tehtud
		System.out.println("print something here");
		int arv; // muutuja deklareerimine
		arv = 7; // muutuja väärtustamine
		System.out.println(arv); // väärtuse avaldamine
		
		int[] arvud = new int[10]; // muutuja deklareerimine näiteks kümne arvu kaupa 
		arvud[0] = 7; // massiivi ühe elemendi väärtustamine
		arvud[1] = 8;
		
		System.out.println(arvud[1]);

	}

}
