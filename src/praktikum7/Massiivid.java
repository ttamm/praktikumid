package praktikum7;

public class Massiivid {

	public static void main(String[] args) {
		int[] arvud = new int[10]; // massiivi deklaratsioon, võimalik panna
									// rohkem elemente = deklareerib 10ne inti'i
									// suuruse väärtuse
		arvud[0] = 7; // massiivi väärtustamine

		System.out.println(arvud[0]); // massiivi väljastamine

		System.out.println(arvud.length); // massiivi pikkus (antud juhul kümme)

		for (int i = 0; i < arvud.length; i++) {
			System.out.println(arvud[i]);
		}
	}

	public static void tryki(int[] massiiv) {
		System.out.print("[");
		for (int i = 0; i < massiiv.length; i++) {
			if (i > 0)
				System.out.print(", ");
			System.out.print(massiiv[i]);
		}
		System.out.print("]");
	}
}
