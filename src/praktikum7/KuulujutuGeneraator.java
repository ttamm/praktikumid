package praktikum7;

public class KuulujutuGeneraator {

	public static void main(String[] args) {
		String[] naised = {"Teele", "Kersti", "Sofia"};
		String[] mehed = {"Sander", "Toomas", "Allan"};
		String[] tegusõnad = {"ja", "või", "ning"};
		
		int suvalineN = (int)(Math.random() * naised.length);
		int suvalineM = (int)(Math.random() * mehed.length);
		int suvalineT = (int)(Math.random() * tegusõnad.length);
		
		System.out.print(naised[suvalineN] + " " + tegusõnad[suvalineT] + " " + mehed[suvalineM]+ ".");

	}
	

}
