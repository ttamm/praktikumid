package praktikum10;

public class FibonacciArv {

	//ka rekursioon
	
	public static void main(String[] args) {
		int i = 1;
		while (true) {
			System.out.println(i + "-" + fibonacci(i));
			i++;
		}}
		
		
		//System.out.println(fibonacci(9));
		// alustab ühest, mitte nullist

	//}

	public static int fibonacci(int n) {
		// tingimused, mille korral programm peab vastavalt käituma
		if (n <= 1) {
			return 1;
		} else {
			return (fibonacci(n - 1) + fibonacci(n - 2));
		}
	}

}
