package praktikum10;


public class Rekursioon {

	public static void main(String[] args) {
				
		System.out.println(astenda(2, 3));
	
	}
	public static int astenda(int arv, int aste) {
		// kirjutada siia blokki lõpetamistingimused
		if (aste == 1 || aste == 0) {
			return arv;
		}else {
			return arv * astenda(arv, aste - 1);
		}

	}

}
