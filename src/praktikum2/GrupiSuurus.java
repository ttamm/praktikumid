package praktikum2;

import lib.TextIO;

public class GrupiSuurus {
	public static void main(String[] args) {
		int arv1;
		System.out.println("Palun sisestage inimeste arv");
		arv1 = TextIO.getlnInt();
		int arv2;
		System.out.println("vajalik grupisuurus");
		arv2 = TextIO.getlnInt();
		System.out.println("Saab moodustada " + (arv1 / arv2) + " gruppi");
		System.out.println("Üle jääb " + arv1 % arv2 + " inimest");
		
	}

}
