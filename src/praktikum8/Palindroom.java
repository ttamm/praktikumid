package praktikum8;

import lib.TextIO;

public class Palindroom {

	public static void main(String[] args) {
		
		// mida teha - Kirjutada programm, mis küsib 
		// kasutajalt sõna ja kontrollib, kas see on 
		// edaspidi ja tagurpidi lugedes sama (palindroom).

		
		System.out.println("Palun sisestage sõna.");
		String sõna = TextIO.getln();
		String sõna1 = new StringBuilder(sõna).reverse().toString();
				
		if(sõna.equals(sõna1)) {
			System.out.println("Yes, it is a palindrome.");
		} else {
			System.out.println("No, it is not a palindrome.");
		}
	}

}
