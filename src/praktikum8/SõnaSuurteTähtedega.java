package praktikum8;

import lib.TextIO;

public class SõnaSuurteTähtedega {

	public static void main(String[] args) {

		// mida teha - Kirjutada programm,
		// mis küsib kasutaja käest sõna ning
		// trükib välja selle sõna suurte tähtedega
		// ning tähed sidekriipsudega eraldatud.
		// Näiteks, kui kasutaja sisestab "Teretore",
		// trükib programm "T-E-R-E-T-O-R-E".

		System.out.println("Palun sisestage sõna.");
		String sõna = TextIO.getln();
		System.out.println("Sisestatud sõna suurte tähtedega: ");
		System.out.println();
		System.out.println(sõna.toUpperCase());

		for (int i = 0; i < sõna.length(); i++) {
			if (i != sõna.length()) {
				System.out.print(sõna.charAt(i) + "-");
			}

		} // POOLIK - viimane kriips ei kao ära

	}

}
