package praktikum8;

import lib.TextIO;

public class KümmeSõna {
	
	// mida teha - kirjutada programm, mis küsib 
	// kasutaja käest kümme sõna ja trükib välja 
	// sõna pikkuse ja sõna enda.

	public static void main(String[] args) {
		System.out.println("Sisestage kümme sõna");
		String[] sõnad = new String[10];

		for (int i = 0; i < sõnad.length; i++) {
			sõnad[i] = TextIO.getln();
			System.out.println(sõnad[i] + " " + sõnad[i].length());

		}
		if (sõnad.length == 10) {
			System.out.println("Done!");
		}

	}
}
