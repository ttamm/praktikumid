package praktikum11;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

public class ValgestMustaks extends Applet {

	@Override
	public void paint(Graphics g) {

		int w = getWidth();
		double h = getHeight();
		
		double värvimuutus = 255 / h ;
		

		for (int i = 0; i < h; i++) {
			
			int värvitoon = (int) (255 - värvimuutus * i);
			Color minuvärv = new Color(värvitoon, värvitoon, värvitoon);
			
			g.setColor(minuvärv);
			g.drawLine(0, i, w, i);
		}
	}

}
