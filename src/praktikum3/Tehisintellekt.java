package praktikum3;

import lib.TextIO;

public class Tehisintellekt {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Sisestage kaks vanust");
		int vanus1;
		int vanus2;
		vanus1 = TextIO.getlnInt();
		vanus2 = TextIO.getlnInt();
		
		if (vanus1 < 0 || vanus2 < 0) {
			System.out.println("ei sobi - vigane");
		} 
				
		int vanusevahe = Math.abs(vanus1 - vanus2);
		

		if (vanusevahe > 5) {
			System.out.println("vanusevahe üle viie");
		}
		if (vanusevahe > 10) {
			System.out.println("vanusevahe üle kümne ");
		}
		if (vanusevahe < 5) {
			System.out.println("vanusevahe alla viie");
			
		}
	}

}
