package praktikum5;

import lib.TextIO;

public class KaksVahemikku {

	public static void main(String[] args) {
		int max;
		int min;
		int sisestatudArv;
		System.out.println("sisesta arv");
		max = 25;
		min = 6;
		sisestatudArv = TextIO.getlnInt();
		while (true) {
			if (sisestatudArv < min || sisestatudArv > max) {
				System.out.println("sisesta arv uuesti");
				sisestatudArv = TextIO.getlnInt();
			} else {
				System.out.println("õige - kasutaja sisestas: " + sisestatudArv);
				break;
			}
		}
	}
}
